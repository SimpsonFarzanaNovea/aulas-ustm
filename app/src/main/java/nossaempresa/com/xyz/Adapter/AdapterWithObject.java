package nossaempresa.com.xyz.Adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import nossaempresa.com.xyz.DatabaseActivity;
import nossaempresa.com.xyz.Model.Pessoa;
import nossaempresa.com.xyz.R;

public class AdapterWithObject extends RecyclerView.Adapter<AdapterWithObject.MyViewHolder> {

    private List<Pessoa> pessoaList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvNome;

        public MyViewHolder(View view) {
            super(view);
            tvNome = (TextView) view.findViewById(R.id.info_text);

            //Metodo para colocar acção no item do RecyclerView
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(null, DatabaseActivity.class);

                }
            });
        }
    }


    public AdapterWithObject(List<Pessoa> pessoaList) {
        this.pessoaList = pessoaList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_pessoa, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Pessoa movie = pessoaList.get(position);
        holder.tvNome.setText(movie.getNome());
    }

    @Override
    public int getItemCount() {
        return pessoaList.size();
    }
}
