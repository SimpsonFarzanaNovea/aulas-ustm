package nossaempresa.com.xyz.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import nossaempresa.com.xyz.Model.Pessoa;

public class DatabaseAdapter {
    DatabaseHelper helper;

    public DatabaseAdapter(Context context) {
        helper = new DatabaseHelper(context);
    }

    public long insertName(String name) {
        SQLiteDatabase dbb = helper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PersonContract.PersonEntry.COLUMN_NAME_NAME, name);
        long id = dbb.insert(PersonContract.PersonEntry.TABLE_NAME, null, contentValues);
        dbb.close();
        return id;
    }

    public List<Pessoa> getPersons() {

        List<Pessoa> pessoas = new ArrayList<>();
        SQLiteDatabase db = helper.getWritableDatabase();

        //Colunas por ler
        String[] projection = {
                PersonContract.PersonEntry._ID,
                PersonContract.PersonEntry.COLUMN_NAME_NAME
        };
        //Query
        Cursor cursor = db.query(PersonContract.PersonEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null);

        while (cursor.moveToNext()) {

            int cid = cursor.getInt(cursor.getColumnIndex(PersonContract.PersonEntry._ID));
            String name = cursor.getString(cursor.getColumnIndex(PersonContract.PersonEntry.COLUMN_NAME_NAME));

            Pessoa pessoa = new Pessoa();
            pessoa.setIt(cid);
            pessoa.setNome(name);
            pessoas.add(pessoa);
        }
        return pessoas;
    }

    public int updatePerson(String oldName, String newName) {
        SQLiteDatabase db = helper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(PersonContract.PersonEntry.COLUMN_NAME_NAME, newName);

        String[] whereArgs = {oldName};

        int count = db.update(PersonContract.PersonEntry.TABLE_NAME,
                contentValues,
                PersonContract.PersonEntry.COLUMN_NAME_NAME + " = ?",
                whereArgs);
        return count;
    }

    public int deletePerson(String name) {
        SQLiteDatabase db = helper.getWritableDatabase();
        String[] whereArgs = {name};

        int count = db.delete(PersonContract.PersonEntry.TABLE_NAME,
                PersonContract.PersonEntry.COLUMN_NAME_NAME + " = ?",
                whereArgs);
        return count;
    }
}
