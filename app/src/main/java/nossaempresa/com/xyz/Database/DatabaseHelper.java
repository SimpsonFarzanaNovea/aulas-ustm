package nossaempresa.com.xyz.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    //Definir o nome da BD
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Person.db";

    //Definir tipos de dados
    private static final String TEXT_TYPE = " TEXT";

    //Comando para escrever na BD
    private static final String SQL_CREATE_PERSON =
            "CREATE TABLE " + PersonContract.PersonEntry.TABLE_NAME + " (" +
                    PersonContract.PersonEntry._ID + " INTEGER PRIMARY KEY," +
                    PersonContract.PersonEntry.COLUMN_NAME_NAME + TEXT_TYPE + " )";

    //Comando que valida se a BD existe ou não
    private static final String SQL_DELETE_PERSON =
            "DROP TABLE IF EXISTS " + PersonContract.PersonEntry.TABLE_NAME;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_PERSON);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_PERSON);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

}
