package nossaempresa.com.xyz;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import nossaempresa.com.xyz.Database.DatabaseAdapter;
import nossaempresa.com.xyz.Database.DatabaseHelper;

public class DatabaseActivity extends AppCompatActivity {

    private EditText etName;
    private Button btAdd, btRead, btUpdate, btDelete;

    DatabaseAdapter helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Inicializar a classe Adapter da BD
        helper = new DatabaseAdapter(getApplicationContext());

        etName = findViewById(R.id.etName);

        btAdd = findViewById(R.id.btAdd);
        btRead = findViewById(R.id.btList);
        btUpdate = findViewById(R.id.btUpdate);
        btDelete = findViewById(R.id.btDelete);

        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = etName.getText().toString();

                long id = helper.insertName(name);

                if (id <= 0) {
                    Toast.makeText(DatabaseActivity.this, "Sucesso", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(DatabaseActivity.this, "Sucesso", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(DatabaseActivity.this, SegundaActivity.class);
                startActivity(i);
            }
        });

        btUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = etName.getText().toString();

                int a = helper.updatePerson("Sousa", name);

                if (a <= 0) {
                    Toast.makeText(DatabaseActivity.this, "Unsuccessful", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(DatabaseActivity.this, "Updated", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = etName.getText().toString();

                int a = helper.deletePerson(name);

                if (a <= 0) {
                    Toast.makeText(DatabaseActivity.this, "Unsuccessful", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(DatabaseActivity.this, "Deleted", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
